import paramiko
import time
import getpass
from tqdm import tqdm

# hostname = str(input("Hostname : "))
# username = str(input("Username : "))
# password = str(input("Password : "))

# if len(hostname) == 0 and len(username) == 0 and len(password) == 0:
hostname = "[fe80::a00:27ff:fef5:2613%7]"
username = 'dimas'
password = 'dimas'

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname, username=username, password=password, allow_agent=False, look_for_keys=False)

print(f"\nSuccesfully Login as {username}")
time.sleep(2)
print("Starting Mikrotik Network Automation")
time.sleep(1)
print("*"*50)

def interface_tab():
    interfacetab = [
        "interface set ether1 name=Ether-1-Biznet",
        "interface set ether2 name=Ether-2-Other",
        "interface set ether3 name=Ether-3-Hypernet",
        "interface set ether4 name=Ether-4-Synology",
        "interface set ether5 name=Deskera-Lan-1 comment=Ether-5",
        "interface set ether6 name=Ether-6",
        "interface set ether7 name=Deskera-Lan-2 comment=Ether-7",
        "interface set ether name=Combo-1-Lokal comment=Ether-Combo-Failover",
        "interface bridge add name=Bridge-1-Lokal fast-forward=yes arp=enabled protocol-mode=none",
        "interface bridge add name=Bridge-Public-Hypernet fast-forward=yes arp=enabled comment=Hypernet",
        "interface bridge add name=Bridge-Public-Biznet fast-forward=yes arp=enabled comment=Biznet",
        "interface bridge port add interface=Deskera-Lan-1 bridge=Bridge-Public-Hypernet",
        "interface bridge port add interface=Ether-2-Other bridge=Bridge-Public-Hypernet",
        "interface bridge port add interface=Ether-3-Hypernet  bridge=Bridge-Public-Hypernet",
        "interface bridge port add interface=Ether-4-Synology  bridge=Bridge-Public-Hypernet",
        "interface bridge port add interface=Deskera-Lan-2 bridge=Bridge-Public-Biznet",
        "interface bridge port add interface=Ether-1-Biznet bridge=Bridge-Public-Biznet",
        "interface bridge port add interface=Combo-1-Lokal bridge=Bridge-1-Lokal",
        "interface bridge port add interface=Ether-6 bridge=Bridge-1-Lokal",
        "interface eoip add name=Eoip-Pusat mac-address=02:FF:64:08:E1:CE arp=enabled remote-address=43.252.74.62 tunnel-id=112 dscp=inherit dont-fragment=no allow-fast-path=yes clamp-tcp-mss=yes comment=Koneksi-Kantor-Pusat ",
        "interface vlan add name=Vlan-30-Via-Hypernetarp=enabled vlan-id=30 interface=Bridge-1-Lokal comment=AP-Owner",
        ""
    ]

    for config in tqdm(interfacetab):
        ssh_client.exec_command(config)
        time.sleep(0.5)

    print("\n")
    print("-" * 7, "Successfully Configuration Interface Tab", "-" * 7)
    print("*" * 50)

    choose()

def ip_tab():
    iptab = [
        "ip address add address=202.93.224.114/29 network=202.93.224.112 interface=Bridge-Public-Hypernet comment=IP-Public-Hypernet",
        "ip address add address=172.17.0.46/30 network=172.17.0.44 interface=Eoip-Pusat",
        "ip address add address=112.78.134.250/29 network=112.78.134.248 interface=Bridge-Public-Biznet comment=IP-Public-Biznet",
        "ip address add address=10.10.12.1/24 network=10.10.12.0 interface=Bridge-1-Lokal comment=IP-AP",
        "ip address add address=192.168.0.8/24 network=192.168.0.0 interface=Bridge-1-Lokal",
        "ip address add address=10.12.2.1/30 network=10.12.2.0 interface=Vlan-30-Via-Hypernet comment=IP-AP-Owner",
        "ip dns set servers=43.252.74.29,114.129.23.33,203.142.82.222,203.142.84.222 allow-remote-requests=yes",
        "ip service disable api,api-ssl,ftp,www",
        "ip service set winbox address=43.252.74.0/24,120.29.152.160/29,10.0.0.0/8,120.29.159.216/30,122.102.44.82,192.168.0.0/16,192.168.0.0/24 port=8666",
        "ip service set telnet port=2323 address=120.29.152.160/29,10.0.0.0/8,120.29.159.216/30,122.102.44.82,192.168.0.0/16",
        "ip service set ssh port=2222 address=120.29.152.160/29,10.0.0.0/8,120.29.159.216/30,122.102.44.82,192.168.0.0/16",
        "ip hotspot profile add name=hsprof1 hotspot-address=192.168.0.8 dns-name=PF.id html-directory=hotspot mac-auth-mode=mac-as-username",
        "ip hotspot profile add name=hsprof2 hotspot-address=10.10.20.1 dns-name=PF.hyp.id html-directory=hotspot",
        "ip hotspot profile add name=hsprof3 hotspot-address=10.5.50.1 html-directory=hotspot",
        "ip hotspot user profile add name=1M address-pool=none session-timeout=10:00:00 shared-users=2 rate-limit=1M/1M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=2M address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=2 rate-limit=2M/2M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=3M address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=2 rate-limit=3M/3M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=5M address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=2 rate-limit=5M/5M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=5M-3U address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=3 rate-limit=5M/5M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=512K-HP address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=1 rate-limit=512k/512k add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=512K-2-User address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=2 rate-limit=512k/512k add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=512K-4-User address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=4 rate-limit=512k/512k add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=Deskera-User address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=2 rate-limit=10M/10M add-mac-cookie=yes mac-cookie-timeout=3d",
        "ip hotspot user profile add name=Unlimited address-pool=none session-timeout=10:00:00 idle-timeout=none status-autorefresh=00:01:00 shared-users=5 rate-limit=10M/10M add-mac-cookie=yes mac-cookie-timeout=3d transparent-proxy=yes",
        "ip hotspot ip-binding add mac-address=6C:3B:6B:8E:D5:4E address=192.168.0.236 server=all type=bypassed comment=CRS",
        "ip hotspot ip-binding add address=192.168.0.180 server=all type=bypassed comment=Fotokopi-MTC-2",
        "ip hotspot ip-binding add mac-address=10:1F:74:70:EF:A0 address=192.168.0.90 server=all type=bypassed comment=Plotter-PDE",
        "ip hotspot ip-binding add address=192.168.0.80 server=all type=bypassed comment=Fotokopi-MTC",
        "ip hotspot ip-binding add address=192.168.0.73 server=all type=bypassed comment=Fotokopi-PDE",
        "ip hotspot ip-binding add mac-address=9C:AE:D3:F9:7F:E7 address=192.168.0.36 server=all type=bypassed comment=Printer-Finance",
        "ip hotspot ip-binding add address=192.168.0.12 server=all type=bypassed comment=Proyektor-PPIC",
        "ip hotspot ip-binding add address=192.168.0.9 server=all type=bypassed comment=Synology",
        "ip hotspot ip-binding add address=192.168.0.8 server=all type=bypassed",
        "ip hotspot ip-binding add mac-address=CA:87:5F:A6:57:2C address=192.168.0.7 server=all type=bypassed comment=Fingerspot-Staff",
        "ip hotspot ip-binding add address=192.168.0.6 server=all type=bypassed comment=DVR",
        "ip hotspot ip-binding add mac-address=B4:A3:82:D6:61:A6 address=192.168.0.5 server=all type=bypassed comment=DVR",
        "ip hotspot ip-binding add mac-address=B2:B8:1F:90:71:25 address=192.168.0.4 server=all type=bypassed comment=Fingerspot-Mesin-2",
        "ip hotspot ip-binding add address=192.168.0.3 server=all type=bypassed comment=Domain-Server",
        "ip hotspot ip-binding add address=192.168.0.2 to-address=192.168.0.2 server=all type=bypassed comment=Deskera",
        "ip hotspot ip-binding add address=10.12.2.0/24 server=all type=bypassed comment=Aruba",
        "ip hotspot ip-binding add address=10.10.12.0/24 server=all type=bypassed comment=Aruba",
        "ip hotspot ip-binding add mac-address=80:8D:B7:CB:0F:9E server=all type=bypassed comment=Aruba-Owner",
    ]

    for config in tqdm(iptab):
        ssh_client.exec_command(config)
        time.sleep(0.5)

    print("\n")
    print("-" * 7, "Successfully Configuration IP Tab", "-" * 7)
    print("*" * 50)

    choose()

def user_hotspot_tab():
    user_hotspot = [
        "ip hotspot user add name=msahlil password=sahlil@1234 profile=1M server=all",
        "ip hotspot user add name=aga password=pde@004 profile=1M server=all",
        "ip hotspot user add name=sutanta password=mtc@01 profile=2M server=all",
        "ip hotspot user add name=nhidayah password=purchase@03 profile=3M server=all",
        "ip hotspot user add name=dwi password=costing@01 profile=3M server=all",
        "ip hotspot user add name=dkrisna password=vier@01 profile=5M server=all",
        "ip hotspot user add name=debbie password=whs@2019 profile=5M server=all",
        "ip hotspot user add name=ponco password=produksi@pf profile=512K-HP server=all",
        "ip hotspot user add name=hartono password=packing@2020 profile=512K-HP server=all",
        "ip hotspot user add name=finadmin password=ptpf@2019 profile=512K-HP server=all",
        "ip hotspot user add name=cromandy password=qc@ptpf profile=512K-HP server=all",
        "ip hotspot user add name=surono password=produksi@pf profile=512K-HP server=all",
        "ip hotspot user add name=nursidi password=ptpf@2020 profile=512K-HP server=all",
        "ip hotspot user add name=sulistyawan password=pde@24 profile=512K-HP server=all",
        "ip hotspot user add name=wagiyanto password=pde@006 profile=512K-HP server=all",
        "ip hotspot user add name=sgunawan password=div2@ptpf profile=512K-HP server=all",
        "ip hotspot user add name=aris password=finishing@20 profile=512K-HP server=all",
        "ip hotspot user add name=effendi password=effendi@2020 profile=512K-HP server=all",
        "ip hotspot user add name=sujono password=qc@1972 profile=512K-HP server=all",
        "ip hotspot user add name=bsetyawan password=finmil@2019 profile=512K-HP server=all",
        "ip hotspot user add name=turwansari password=pde@2018 profile=512K-2-User server=all",
        "ip hotspot user add name=mlatif password=div1@01 profile=512K-2-User server=all",
        "ip hotspot user add name=mudhalifah password=ifa@2345 profile=512K-2-User server=all",
        "ip hotspot user add name=nuryadi password=cost@2020 profile=512K-2-User server=all",
        "ip hotspot user add name=ratih password=mtc@2019 profile=512K-2-User server=all",
        "ip hotspot user add name=ulfaizah password=qa@212 profile=512K-2-User server=all",
        "ip hotspot user add name=security password=bravo001 profile=512K-4-User server=all",
        "ip hotspot user add name=tarwanto password=pakt@001 profile=Deskera-User server=all",
        "ip hotspot user add name=pipit password=qc@2019 profile=Deskera-User server=all",
        "ip hotspot user add name=rochmani password=produksi@tr profile=Deskera-User server=all",
        "ip hotspot user add name=pprabowo password=pde@2019 profile=Deskera-User server=all",
        "ip hotspot user add name=melisa password=melisa@ppic profile=Deskera-User server=all",
        "ip hotspot user add name=niam password=wrh@2019 profile=Deskera-User server=all",
        "ip hotspot user add name=syahrul password=ppic@2020 profile=Deskera-User server=all",
        "ip hotspot user add name=ulfa password=produksi@tr profile=Deskera-User server=all",
        "ip hotspot user add name=lshodiqin password=ptpf@2020 profile=Deskera-User server=all",
        "ip hotspot user add name=dbcabucos password=november@12 profile=Unlimited server=all",
        "ip hotspot user add name=khoiri password=finance@002 profile=Unlimited server=all",
        "ip hotspot user add name=alistiani password=hrd@01 profile=Unlimited server=all",
        "ip hotspot user add name=rtbadayos password=production@01 profile=Unlimited server=all",
        "ip hotspot user add name=nurul password=ptpf@2019 profile=Unlimited server=all",
        "ip hotspot user add name=vanny password=purchase@02 profile=Unlimited server=all",
        "ip hotspot user add name=nraharto password=kreo@18 profile=Unlimited server=all",
        "ip hotspot user add name=adinasari password=finance@003 profile=Unlimited server=all",
        "ip hotspot user add name=maryono password=ppic@2019 profile=Unlimited server=all",
        "ip hotspot user add name=cielo password=ptmc@2020 profile=Unlimited server=all",
        "ip hotspot user add name=itsupport password=Fadhian1 profile=Unlimited server=all",
        "ip hotspot user add name=dlestari password=@dmin123 profile=Unlimited server=all",
        "ip hotspot user add name=itpc password=pentium@32 profile=Unlimited server=all",
        "ip hotspot user add name=customer password=ptmc@2020 profile=Unlimited server=all",
        "p hotspot user add name=cms password=cms@001 profile=Unlimited server=all",
        "ip hotspot user add name=divina password=ptmc@2019 profile=Unlimited server=all",
        "ip hotspot user add name=mareciel password=od@01 profile=Unlimited server=all",
        "ip hotspot user add name=tope password=tope profile=Unlimited server=all",
        "ip hotspot user add name=venia password=venia profile=Unlimited server=all",
        "ip hotspot user add name=ydarwanto password=Beta123 profile=Unlimited server=all",
        "ip hotspot user add name=ahmad password=Beta123 profile=Unlimited server=all",
    ]

    for config in tqdm(user_hotspot):
        ssh_client.exec_command(config)
        time.sleep(0.5)

    print("\n")
    print("-" * 7, "Successfully Configuration User Hotspot", "-" * 7)
    print("*" * 50)

    choose()

def system_tab():
    systemtab = [
        "system identity set name=Pacific-Funiture",
        "user add name=dimas password=dimas group=full comment=it-supp-new",
        "user add name=arya password=pentium_cll230 group=full comment=it-supp-old",
        "user disable admin",
    ]

    for config in tqdm(systemtab):
        ssh_client.exec_command(config)
        time.sleep(0.5)

    print("\n")
    print("-" * 7, "Successfully Configuration System Tab", "-" * 7)
    print("*" * 50)

    choose()

def queues_tab():
    queuestab = [
        "queue simple add name=hs-<hotspot-biznet> target=Bridge-1-Lokal",
        "queue tree add name=global-traffic parent=global",
        "queue tree add name=no-mark-conn parent=global-traffic packet-mark=no-mark",
        "queue tree add name=wan1-marked-conn parent=global-traffic",
        "queue tree add name=globaldownload parent=wan1-marked-conn queue=pcq-download-default ",
        "queue tree add name=othersdownload parent=globaldownload queue=pcq-download-default priority=2",
        "queue tree add name=deskeradown parent=othersdownload packet-mark=deskeradown queue=pcq-download-default priority=2 max-limit=50M",
        "queue tree add name=emaildown parent=othersdownload packet-mark=emaildown queue=pcq-download-default priority=2 max-limit=50M",
        "queue tree add name=zoomdown parent=othersdownload packet-mark=paket_zoom queue=pcq-download-default priority=2 max-limit=50M",
        "queue tree add name=globalupload parent=wan1-marked-conn queue=pcq-download-default",
        "queue tree add name=deskeraup parent=globalupload packet-mark=deskeraup queue=pcq-upload-default priority=2 max-limit=50M",
        "queue tree add name=emailup parent=globalupload packet-mark=emailup queue=pcq-upload-default priority=2 max-limit=50M",
        "queue tree add name=othersupload parent=globalupload queue=pcq-upload-default priority=2",
        "queue tree add name=zoomup parent=othersupload packet-mark=paket_zoom queue=pcq-upload-default priority=2 max-limit=30M",
    ]

    for config in tqdm(queuestab):
        ssh_client.exec_command(config)
        time.sleep(0.5)

    print("\n")
    print("-" * 7, "Successfully Configuration Queues Tab", "-" * 7)
    print("*" * 50)

    choose()

def configureSwitch():
    pass

def resetRouter():
    # config = "system reset-configuration no-defaults=yes"
    # ssh_client.exec_command(config)
    pass

    choose()

def quit():
    quit()

def menu():
    print("""
    NB : 1. Interface-Tab
         2. IP-Tab
         3. System-Tab
         4. Queues-Tab
         5. User Hotspot
         5. Quit
    Choose with Integer/Number
        """)
    choose()

def choose():
    device = (input("> ")).lower()

    if device == '1':
        interface_tab()
    elif device == '2':
        ip_tab()
    elif device == '3':
        system_tab()
    elif device == '4':
        queues_tab()
    elif device == '5':
        user_hotspot_tab()
    elif device == '6':
        quit()
    elif device == "menu":
        menu()
    else:
        print("Something Went Wrong")
        choose()


choose()