# Write a function, persistence, that takes in a positive parameter num
# and returns its multiplicative persistence, which is the number of times you must multiply
# the digits in num until you
# reach a single digit.
step = 0
def persistence(n):
    # your code
    global step
    result = 1

    for i in str(n):
        result = result * int(i)
        step += 1

    if len(str(result)) > 1:
        persistence(result)
    else:
        print(step)

persistence(433)