def to_weird_case_word(string):
    # print("".join(c.upper() if i % 2 == 0 else c for i, c in enumerate(string.lower())))

    result = ''
    for i, c in enumerate(string.lower()):
        # print(i, c)
        if i % 2:
            result += c.upper()
        else:
            result += c

    return result

to_weird_case_word('this')