def solution(s):
    n = 2
    s = s.replace(" ", "")
    out = [(s[i:i+n]) for i in range(0, len(s), n)]
    result = []
    for i in out:
        if len(i) < 2:
            result.append(i+"_")
        else:
            result.append(i)

    return result

#   Best Practice
    import re

    def solution(s):
        return re.findall(".{2}", s + "_")

solution("dimas okva solichin")