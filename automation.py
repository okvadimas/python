import paramiko
import time
import pandas

hostname = "08:00:27:F5:26:13"
username = "admin123"
password = "admin123"

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname, username=username, password=password, port=22, allow_agent=False, look_for_keys=False)

print(f"\nSuccessfully Login as {username}")

configs = [
    "ip hotspot active print stats where user=msahlil",
]

for config in configs:
    print(getattr(ssh_client.exec_command(config)))
    # print(result)

