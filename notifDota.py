from selenium import webdriver
from time import sleep
import time
from bs4 import BeautifulSoup
import requests

def run():

    link = [
        "https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/Europe/Upper_Division",
        "https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/China/Upper_Division",
        "https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/Southeast_Asia/Upper_Division",
        "https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/Southeast_Asia/Lower_Division",
        "https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/North_America/Upper_Division"
    ]

    URL = 'https://liquipedia.net/dota2/Dota_Pro_Circuit/2021/1/Europe/Upper_Division'
    page = requests.get(URL)

    soup = BeautifulSoup(page.content, 'html.parser')
    dd = soup.find_all(class_='team-template-text')

    for d in dd:
        print(d.find())

run()