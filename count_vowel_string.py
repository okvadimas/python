def get_count(input_str):
    num_vowels = 0
    # your code here
    vowel = ['a', 'i', 'u', 'e', 'o']
    for i in input_str:
        if i in vowel:
            num_vowels += 1

    return num_vowels

    # best practice
    return sum(c in 'aeiou' for c in input_str)

get_count("aiueo")