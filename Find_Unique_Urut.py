def longest(s1, s2):
    # My Stupid Brain Logic
    new = s1 + s2
    new = sorted(set(new))
    result = ""
    for i in new:
        result += i

    return result

# Best Practice
def longest(a1, a2):
    result = "".join(sorted(set(a1 + a2)))
    return result


a = "dawdnlanfaiwnhdo"
b = "dawdnjawdwuajdnaw"

longest(a, b)
