def order(sentence):
    sentence = sentence.split(" ")
    d = {}
    for i in sentence:
        for j in i:
            if j in "123456789":
                d[j] = i

    result = ""
    for i in sorted(d.keys()):
        result += f"{d[i] + ' '}"

    return result.strip()

#   Best Practice
    return ' '.join(sorted(words.split(), key=lambda w:sorted(w)))

order("4of Fo1r pe6ople g3ood th5e the2")
