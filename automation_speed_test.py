from selenium import webdriver
from time import sleep
import time
import os

test_time = ["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00"]

Speed_today = []

name_of_file = time.strftime("%A, %B %d %Y")

if os.path.isfile("D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\" + name_of_file + ".txt") == False:
    file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'w+')
    file.close()

def test():

    bot = webdriver.Chrome("D:\Dimas\chromedriver.exe")
    bot.get("https://www.speedtest.net/")
    bot.maximize_window()

    # Change Server
    bot.find_element_by_class_name("start-text").click()
    # if "result-data-large number result-data-value download-speed" == True:

    sleep(100)

    download_speed = bot.find_element_by_class_name("download-speed")
    download_speed = download_speed.get_attribute("innerHTML")
    download_speed = round(float(download_speed))

    upload_speed = bot.find_element_by_class_name("upload-speed")
    upload_speed = upload_speed.get_attribute("innerHTML")
    upload_speed = round(float(upload_speed))

    convert_speed = round((download_speed + upload_speed) / 2)

    Speed_today.append(convert_speed)
    print(Speed_today)

    name_of_file = time.strftime("%A, %B %d %Y")

    print(testing_time)

    if testing_time == "08:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"08:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "09:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"09:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "10:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"10:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "11:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"11:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "12:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"12:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "13:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"13:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "14:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"14:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "15:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"15:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "16:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"16:00 = {convert_speed}\n")
        file.close()
    elif testing_time == "17:00":
        file = open(f'D:\\Dimas\\python file\\Python Project\\Monitoring Speed\\{name_of_file}.txt', 'a+')
        file.write(f"17:00 = {convert_speed}\n")
        file.close()

    bot.close()
    bot.quit()


while True:
    testing_time = time.strftime("%H:%M")
    if testing_time in test_time:
        try:
            test()
        except:
            test()
    else:
        sleep(1)