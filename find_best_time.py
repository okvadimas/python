def is_valid_walk(walk):
    #determine if walk is valid
    result = ""
    if len(walk) == 10 and walk.count("n") == walk.count("s") and walk.count("e") == walk.count("w"):
        result = True
    else:
        result = False

    return result

#   Best Practice
    return len(walk) == 10 and walk.count('n') == walk.count('s') and walk.count('e') == walk.count('w')

is_valid_walk(['n','n','n','s','n','s','n','s','n','s'])

